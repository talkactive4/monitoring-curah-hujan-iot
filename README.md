Kode perangkat IoT untuk proyek Monitoring Curah Hujan

## Deployment

Kode dapat langsung dimasukkan ke perangkat IoT NodeMCU.

## Built With

* [Arduino IDE](https://www.arduino.cc/) - IDE untuk pengembangan perangkat IOT
* [Firebase](https://firebase.google.com/) - Backend

## Authors

* **Ferian Chandra** - *Initial work* - [Profile](https://gitlab.com/talkactive4)

See also the list of contributors who participated in this project.

## License

This project is licensed under the MIT License - see the LICENSE.md file for details

## Acknowledgments

* [Readme.md template](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2)
