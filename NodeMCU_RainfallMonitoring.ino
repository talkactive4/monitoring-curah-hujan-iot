#include <ESP8266WiFi.h>          // library esp8266
#include "NodeMCUIotDevice.h";

IotDevice node;

void setup(){
  pinMode (node.OutputD8, INPUT) ; //Defisini OutputDO sebagai input
  //inisialisasi komunikasi serial
  Serial.begin(9600);  
  delay(1000);           
                   
  WiFi.begin(node.WIFI_SSID, node.WIFI_PASSWORD);         // mencoba koneksi ke wifi
  Serial.print("Connecting to ");
  Serial.print(node.WIFI_SSID);                

  // Perulangan untuk menunggu status wifi menjadi terkoneksi
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }

  Serial.println();
  Serial.print("Connected to ");
  Serial.println(node.WIFI_SSID);                    // print SSID wifi
  Serial.print("IP Address : ");
  Serial.println(WiFi.localIP());               // print IP Address lokal
}

void loop(){
  node.collectData();

  Serial.print(WiFi.status());
  Serial.print(" | ");
  Serial.println(WL_CONNECTED);
  
  if (WiFi.status() == WL_CONNECTED){
    node.sendCollectedData(); 
  }
}
