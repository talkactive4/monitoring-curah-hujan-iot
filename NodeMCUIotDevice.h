#include <ESP8266HTTPClient.h>

#include <ESP8266WiFi.h>          // library esp8266
#include <Wire.h>

class IotDevice{
  private:
    String VALIDATE_DATA_URL = "http://us-central1-weather-station-sidi2017.cloudfunctions.net/validateSensorDataReq";
    HTTPClient http;
    
    // sensor
    int val;//val sebagai buffer data
    float sensor; //sebagai data buffer sensor

    double totalRain = 0.0;
    double currentRain = 0.0;
    bool bucketPositionA = false;
    const double bucketAmount = 0.053;        // 0.053 inches atau 1.346 mm of rain equivalent of ml to trip tipping-bucket 
    int repeat = 30;


  public:
    
    int OutputD8=15; //nama alias pin GPIO 15 yaitu Output D8
    int OutputAO=A0; //nama alias pin AO yaitu Output AO

    String WIFI_SSID = "HotspotNode";   // nama network/hotspot wifi
    String WIFI_PASSWORD = "kyaaaaah";  // password wifi
    
    void collectData(){
      totalRain = 0;
      currentRain = 0;
      for(int i = 0;i<repeat;i++){
        currentRain = 0;
    
        if ((bucketPositionA==false)&&(digitalRead(OutputD8)==LOW)){
          bucketPositionA = true;
          currentRain = bucketAmount*2.54*10;
          totalRain+=currentRain;                               // update the total rain
        }
        
        if ((bucketPositionA==true)&&(digitalRead(OutputD8)==HIGH)){
          bucketPositionA = false;
          currentRain = 0;
          totalRain+=currentRain;                               // update the total rain
        } 
        
        // fancy display for humans to comprehend
        Serial.print("Repeat-");
        Serial.println(i+1);
        Serial.print("Total curah hujan: ");
        Serial.print(totalRain,3);                            // the '3' ensures the required accuracy digit dibelakang koma
        Serial.println(" mm");
        Serial.print("Curah hujan saat ini: ");
        Serial.print(currentRain,3);                            // the '3' ensures the required accuracy digit dibelakang koma
        Serial.print(" mm");
        Serial.println();
    
        delay(1000);
      }
    }
  
  void sendCollectedData(){
    String statusHujan = "Rendah";
    if(totalRain > 500){
      statusHujan = "Sangat Tinggi";
    } else if((totalRain >= 300)&&(totalRain <= 500)){
      statusHujan = "Tinggi";
    } else if((totalRain >= 100)&&(totalRain < 300)){
      statusHujan = "Menengah";
    } else {
      statusHujan = "Rendah";
    }
    Serial.println("Sending Data");
    // code based from https://stackoverflow.com/questions/57072071/cannot-upload-data-to-firebase-using-nodemcu
    String url = String(VALIDATE_DATA_URL) + "?statusHujan=" + statusHujan
                            + "&curahHujan=" + String(totalRain,3); 
    http.begin(url);
    int httpCode = http.GET();
    String payload = http.getString();
    Serial.println(payload);
    http.end();
  }
  
};
